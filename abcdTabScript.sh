#! /bin/bash

echo "the script started"

INPUT_FILE="testIpnut.keg"
RESULT_FILE="result.txt"

echo "the input file is called '$INPUT_FILE' and the result file is called: '$RESULT_FILE'"

# if file exists, remove it
[ -e "$RESULT_FILE" ] && rm "$RESULT_FILE"
touch "$RESULT_FILE"

varA="";
varB="";
varC="";
varD="";

while read line; 
do 
    case ${line:0:1} in

        "A")
            varA="$line"
            ;;

        "B")
            varB="$line"
            ;;

        "C")
            varC="$line"
            ;;

        "D")
            printf "$varA" >> "$RESULT_FILE"; 
            printf "\t" >> "$RESULT_FILE"; 
            printf "$varB" >> "$RESULT_FILE"; 
            printf "\t" >> "$RESULT_FILE"; 
            printf "$varC" >> "$RESULT_FILE"; 
            printf "\t" >> "$RESULT_FILE";

            printf "$line" >> "$RESULT_FILE"; 
            echo "" >> "$RESULT_FILE";
            ;;
    esac
done < "$INPUT_FILE"

echo "the script finished successfully"
